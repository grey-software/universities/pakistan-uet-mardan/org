---
title: Home 🏠
description: Grey Software's UET Mardan sub-organization Website
category: Info
position: 1
---

**Welcome to Grey Software's UET Mardan sub-organization Website!**

Grey software partnered with UET Mardan to run the pilot iteration of our University Student Open Source Apprentice (USOSA) program. 

### Spring 2021 Batch

**[Apprentices](/spring-2021/)**

**[Staff](/spring-2021/)**

**[Projects](/spring-2021/)**

**[Assignments](/spring-2021/)**

### About USOSA (University Student Open Source Apprentice Program)

Grey Software’s USOSA program is a semester-long internship alternative for students interested in becoming software developers. Students will earn university course credit while working alongside open-source maintainers on projects with real-world impact.

**Educational outcomes**

Students are familiar with collaborative software development using tools like Github, Gitlab, Gitpod, Discord.

Students develop opinions about software development best practises and become comfortable conversing with other engineers around the world about solutions to design and engineering problems.

Students build an impressive profile of open source contributions (Issues and Merge Requests) in an agile software development environment. 

**Onboarding**

We onboarded our students through our onboarding website at https://onboarding.grey.software

**Development Environments**

For our web based projects, we will setup Gitpods with the optimal configurations so our students and mentors can collaborate with ease. 

**_What does optimal configuration mean_**

A configuration where the student can jump straight into coding after launching the development environment. All necessary dependencies should be set up and running or testing scripts available. 