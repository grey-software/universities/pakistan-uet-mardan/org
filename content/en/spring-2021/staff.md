---
title: Staff
description:
position: 3
category: Spring 2021
mentors: 
  - name: Arsala
    avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
    github: https://github.com/ArsalaBangash
    gitlab: https://gitlab.com/ArsalaBangash
    linkedin: https://linkedin.com/in/ArsalaBangash

  - name: Adil Shehzad
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7507821/avatar.png
    github: adilshehzad786
    gitlab: adilshehzad
    linkedin: adilshehzad7
admin: 

  - name: Hamees
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8058458/avatar.png
    position: Administrative Officer
    github: https://github.com/mhamees
    gitlab: https://gitlab.com/mhamees
    linkedin: https://www.linkedin.com/in/muhammad-hamees/

---

### Mentors

<team-profiles :profiles="mentors"></team-profiles>

### Admin

<team-profiles :profiles="admin"></team-profiles>