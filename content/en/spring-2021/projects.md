---
title: Projects
description: Learn about our Spring 2021 USOSA Projects
category: Spring 2021
position: 4
---

## Projects

### Open Governance

Open governance is a suite of modern technologies to reform and digitize local governance being co-developed with the municipal corporation of Muzaffarabad, AJK.

**Technologies You’ll Learn**

HTML, CSS, Javascript, Vue, Typescript, Parse, NodeJS, Express, Tailwind, API Development, Linux, Gitlab

**This project is for you if**

This project is for you if you're passionate about building civic tech solutions and you want to develop your fullstack web app development skills.


[Git Repository](https://gitlab.com/grey-software/open-gov)

### Material Math

A web application that allows you to practice Math problems in a fun, beautiful, material experience. 

[Git Repository](https://gitlab.com/grey-software/material-math)
[Website](https://material-math.grey.software)

### Focused Browsing

Focused Browsing is a web extension that allows you to browse popular websites without elements that are designed to take away your attention and focus. The websites we are currently targeting are LinkedOn, Twitter, Youtube, Facebook, and Instagram.

We are is looking for 1 engineering student to work as the lead maintainer for this project.


**Technologies You'll Learn**
Web Extensions, NodeJS, HTML, CSS, Javascript

**This project is for you if**

This project is for you if you're passionate about building privacy-respecting tech solutions and you want to develop your web development skills.


[Git Repository](https://gitlab.com/grey-software/focused-browsing)
[Website](https://focused-browsing.grey.software)

### Modfy.Video

Modfy allows you to trim, convert and combine videos within your browser without uploading your files anywhere.

**Technologies You’ll Learn**

React, Next Js,Typescript, NodeJS, Express, Tailwind, Electron, Web Assembly(Not too deep, mostly abstracted away), Linux, Git, FFmpeg

**This project is for you if**

This project is for you if you are great at learning technologies fast and shipping product, are excited to see behind the scenes of an early stage startup, albeit a bit unconventional one, and especially if you are interested in video processing.

[Git Repository](https://github.com/modfy/modfy.video)
[Website](https://modfy.video/)

### The Physics Hub

The Physics Hub is a free, open source application that allows everyone to understand physics concepts using clear and comprehensible simulations!


**This project is for you if**

This project is for you if you are interested in developing education technology related to science concepts, and you'd like to experience what it's like working with a client in academia.

[Git Repository](https://github.com/ThePhysHub/ThePhysicsHub)
[Website](https://physicshub.herokuapp.com/)


### CodeDog

[Git Repository](https://github.com/BruceDLong/CodeDog)
[Website](http://theslipstream.com/index.html)